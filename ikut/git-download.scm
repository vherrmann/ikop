(define-module (ikut git-download)
  #:use-module (srfi srfi-1)
  #:use-module (guix git-download)
  #:export (github-reference))


;; This module wraps the (guix git-download)
;; Re-export all bindings from (guix git-download)
;; Tell me if the eval is bad practice or if you know a better way :-)
(module-for-each (lambda (symbol var)
                   (eval `(re-export ,symbol)
                         (current-module)))
                 (resolve-interface '(guix git-download)))

(define-public (github-url owner repo)
  (string-append "https://github.com/"
                 owner
                 "/"
                 repo))

;; Create git-reference
;; (define-syntax github-reference
;;   (lambda (x)
;;     (syntax-case x ()
;;       [(_ (field value) ...)
;;        (let* [(private-fields '(owner repo))
;;               (result
;;                (fold (lambda (fields rest)
;;                        (let* [(private (car rest))
;;                               (other (cdr rest))
;;                               (field (car fields))]
;;                          (cond [(member field private-fields eq?)
;;                                 (cons (let [(res (assq-ref private
;;                                                            field))]
;;                                         ;;throw an error if the key is already in the list
;;                                         (if res
;;                                             (error "duplicate field initializer in form" fields)
;;                                             (cons fields
;;                                                   private)))
;;                                       other)]
;;                                [(eq? 'url field)
;;                                 (error "the url field initializer isn't allowed. Use owner repo")]
;;                                [else (cons private
;;                                            (cons fields
;;                                                  other))])))
;;                      '(() . ())
;;                      (syntax->datum #`((field value) ...))))]
;;          #`(git-reference
;;             (url #,(let [(owner (assq-ref (car result)
;;                                           'owner))
;;                          (repo   (assq-ref (car result)
;;                                            'repo))]
;;                      (if (and owner repo)
;;                          (string-append "https://www.github.com/"
;;                                         (car owner) "/"
;;                                         (car repo))
;;                          (error "missing field initializers"
;;                                 "owner or repo" ;FIXME: find the specific missin ones
;;                                 "in form (github-reference)"))))
;;             #,@(if (assq-ref (cdr result)
;;                              'commit)
;;                    (datum->syntax x (cdr result))
;;                    (error "missing field initializer (commit) in form (github-reference)"))))])))
