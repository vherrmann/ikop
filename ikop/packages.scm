(define-module (ikop packages)
  #:use-module (ice-9 match)
  #:use-module (guix diagnostics)
  #:use-module (guix i18n)
  #:use-module (guix)
  #:export (auto-inputs
            auto-inputs-proc
            search-patch
            search-patches))

(define %distro-root-directory
  ;; Absolute file name of the module hierarchy.  Since (gnu packages …) might
  ;; live in a directory different from (guix), try to get the best match.
  (letrec-syntax ((dirname* (syntax-rules ()
                              ((_ file)
                               (dirname file))
                              ((_ file head tail ...)
                               (dirname (dirname* file tail ...)))))
                  (try      (syntax-rules ()
                              ((_ (file things ...) rest ...)
                               (match (search-path %load-path file)
                                 (#f
                                  (try rest ...))
                                 (absolute
                                  (dirname* absolute things ...))))
                              ((_)
                               #f))))
    (try ("ikop/packages.scm" ikop/))))

(define (search-patch file-name)
  "Search the patch FILE-NAME.  Raise an error if not found."
  (or (search-path `(,(string-append %distro-root-directory
                                     "/ikop/packages/patches"))
                   file-name)
      (raise (formatted-message (G_ "~s: patch not found")
                                file-name))))

(define-syntax-rule (search-patches file-name ...)
  "Return the list of absolute file names corresponding to each
FILE-NAME found in %PATCH-PATH."
  (list (search-patch file-name) ...))

(define-syntax auto-inputs
  (lambda (x)
    (syntax-case x ()
      [(_ inputs ...)
       #``(#,@(map (lambda (arg)
                     (match arg
                       [(st1 id)
                        #`(#,(datum->syntax x st1)
                           ,#,(datum->syntax x id))]
                       [(st1 id st2)
                        #`(#,(datum->syntax x st1)
                           ,#,(datum->syntax x id)
                           #,(datum->syntax x st2))]
                       [pkg
                        #`,(let ([pkg #,(datum->syntax x pkg)])
                             `(,(package-name pkg) ,pkg))]
                       [_
                        (error "the clause" arg "in the form" (syntax->datum x)
                               "has the wrong number of arguments")]))
                   (syntax->datum #`(inputs ...))))])))

(define (auto-inputs-proc . inputs)
  (map (lambda (arg)
         (match arg
           [(? package?)
            (list (package-name arg) arg)]
           [(and (st1 id)
                 ((? string?) (? package?)))
            arg]
           [(and (st1 id st2)
                 ((? string?) (? package?) (? string?)))
            arg]))
            ;; FIXME: throw error
       inputs))
