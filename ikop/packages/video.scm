(define-module (ikop packages video))

(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix git-download)
             (guix build-system cmake)
             (gnu)
             (ikut git-download)
             (ikop packages)
             (ikop packages image-processing))

(use-package-modules video
                     image-processing
                     qt
                     compression
                     graphics
                     pkg-config
                     base)


(define-public olive-editor
  (package
   (name "olive-editor")
   (version "0.2.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url (github-url "olive-editor" "olive"))
           (commit "19eabf283062ed0d046b8ce8dee8a14af7c6de31")))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "092nkhw9dws6969312n5fg05m5yx7nv0c77svbnr15hxl5bn3gkx"))))
   (build-system cmake-build-system)
   (native-inputs (auto-inputs pkg-config
                               which
                               qttools))
   (inputs (auto-inputs ffmpeg
                        zlib
                        frei0r-plugins
                        openexr
                        openimageio
                        opencolorio-2
                        qtbase
                        qtsvg
                        qtmultimedia))
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     (delete 'check) ; No rule to make target 'test'
                     (add-after 'install 'wrap-executable
                                (lambda* (#:key inputs outputs #:allow-other-keys)
                                  (let* ((out (assoc-ref outputs "out"))
                                         (qtbase (assoc-ref inputs "qtbase"))
                                         (frei0r (assoc-ref inputs "frei0r-plugins"))
                                         (ffmpeg (assoc-ref inputs "ffmpeg")))
                                    (wrap-program (string-append out "/bin/olive-editor")
                                                  `("PATH" ":" prefix
                                                    ,(list (string-append ffmpeg "/bin")))
                                                  `("QT_PLUGIN_PATH" ":" prefix
                                                    ,(list (getenv "QT_PLUGIN_PATH")))
                                                  `("FREI0R_PATH" ":" =
                                                    (,(string-append frei0r "/lib/frei0r-1/")))
                                                  `("QT_QPA_PLATFORM_PLUGIN_PATH" ":" =
                                                    (,(string-append qtbase "/lib/qt5/plugins/platforms")))
                                                  `("QML2_IMPORT_PATH" ":" prefix
                                                    ,(list (getenv "QML2_IMPORT_PATH")))))
                                  #t)))))
   (home-page
    "https://github.com/olive-editor/olive")
   (synopsis
    "Professional open-source NLE video editor.")
   (description
    "Professional open-source NLE video editor")
   (license license:gpl3)))
