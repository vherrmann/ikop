(define-module (ikop packages keyboard))

(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix build-system haskell)
             (guix download)
             (gnu packages haskell-web)
             (gnu packages haskell-xyz)
             (gnu packages haskell-check)
             (ikop packages haskell-xyz))

(define-public thock
  (package
    (name "thock")
    (version "0.2.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/thock/thock-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0s5xxmbxpr6g2j7797j8ix51405q7455s74x5dijfpi13phx7v94"))))
    (build-system haskell-build-system)
    (inputs
     `(("ghc-aeson" ,ghc-aeson)
       ("ghc-brick" ,ghc-brick)
       ("ghc-file-embed" ,ghc-file-embed)
       ("ghc-lens" ,ghc-lens)
       ("ghc-network" ,ghc-network)
       ("ghc-random" ,ghc-random)
       ("ghc-text-zipper" ,ghc-text-zipper)
       ("ghc-vector" ,ghc-vector)
       ("ghc-vty" ,ghc-vty)
       ("ghc-websockets" ,ghc-websockets)))
    (home-page
     "https://github.com/rmehri01/thock#readme")
    (synopsis
     "A modern TUI typing game featuring online racing against friends.")
    (description
     "Please see the README on GitHub at <https://github.com/rmehri01/thock#readme>")
    (license license:expat)))


