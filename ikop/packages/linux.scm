(define-module (ikop packages linux))

(use-modules (guix)
             (guix packages)
             (guix build-system gnu)
             (guix download)
             ((guix licenses) #:prefix license:)
             (gnu))

(use-package-modules check
                     pkg-config
                     compression
                     linux
                     autotools)

(define-public kbd
  (package
    (name "kbd")
    (version "2.4.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://kernel.org/linux/utils/kbd/kbd-"
                                  version ".tar.xz"))
              (sha256
               (base32
                "17wvrqz2kk0w87idinhyvd31ih1dp7ldfl2yfx7ailygb0279w2m"))
              (modules '((guix build utils)))
              (snippet
               '(begin
                  (substitute* '("src/unicode_start" "src/unicode_stop")
                    ;; Assume the Coreutils are in $PATH.
                    (("/usr/bin/tty")
                     "tty"))
                  #t))))
    (build-system gnu-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (add-before 'build 'pre-build
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((gzip  (assoc-ref %build-inputs "gzip"))
                   (bzip2 (assoc-ref %build-inputs "bzip2"))
                   (zstd (assoc-ref %build-inputs "zstd"))
                   (xz (assoc-ref %build-inputs "xz")))
               (substitute* "src/libkbdfile/kbdfile.c"
                 (("gzip ")
                  (string-append gzip "/bin/gzip "))
                 (("bzip2")
                  (string-append bzip2 "/bin/bzip2 "))
                 (("zstd")
                  (string-append bzip2 "/bin/zstd "))
                 (("xz")
                  (string-append bzip2 "/bin/xz ")))
               #t)))
         (add-after 'install 'post-install
           (lambda* (#:key outputs #:allow-other-keys)
             ;; Make sure these programs find their comrades.
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin")))
               (for-each (lambda (prog)
                           (wrap-program (string-append bin "/" prog)
                             `("PATH" ":" prefix (,bin))))
                         '("unicode_start" "unicode_stop"))
               #t))))))
    (inputs `(("gzip" ,gzip)
              ("bzip2" ,bzip2)
              ("zstd" ,zstd)
              ("xz" ,xz)
              ("pam" ,linux-pam)))
    (native-search-paths
     (list (search-path-specification
            (variable "LOADKEYS_KEYMAP_PATH")
            ;; Append ‘/**’ to recursively search all directories.  One can then
            ;; run (for example) ‘loadkeys en-latin9’ instead of having to find
            ;; and type ‘i386/colemak/en-latin9’ on a mislabelled keyboard.
            (files (list "share/keymaps/**")))))
    (native-inputs `(("pkg-config" ,pkg-config)
                     ("check" ,check)
                     ("autoconf" ,autoconf)))
    (home-page "http://kbd-project.org/")
    (synopsis "Linux keyboard utilities and keyboard maps")
    (description
     "This package contains keytable files and keyboard utilities compatible
for systems using the Linux kernel.  This includes commands such as
@code{loadkeys}, @code{setfont}, @code{kbdinfo}, and @code{chvt}.")
    (license license:gpl2+)))
