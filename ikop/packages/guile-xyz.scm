(define-module (ikop packages guile-xyz))

(use-modules (guix)
             (guix packages)
             (guix build-system gnu)
             (guix git-download)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages pkg-config))

(define-public guile-fymbol
  (package
    (name "guile-fymbol")
    (version "0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/vherrmann/fymbol")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1m5apqb61xp53cz99rxqssvydxz8la4wyhdhd027ghwckn6j3sa9"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("guile" ,guile-3.0)))
    (arguments
     '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
    (home-page "")
    (synopsis "Fancy Symbols for Guile scheme")
    (description "Fancy Symbols for Guile scheme")
    (license #f)))
