(define-module (ikop packages image-processing))

(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix git-download)
             (guix build-system cmake)
             (gnu)
             (ikop packages c++-xyz))

(use-package-modules ghostscript
                     graphics
                     xml
                     version-control
                     pkg-config
                     serialization
                     python
                     gl
                     python-xyz)

(define-public opencolorio-2
  (package
    (name "opencolorio")
    (version "2.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/AcademySoftwareFoundation/OpenColorIO")
             (commit (string-append "v" version))))
       (sha256
        (base32 "0888fca8wa5zdc6f7lmh1wi7ljw75ql0rlzaslk2zffd08ij0s38"))
       (file-name (git-file-name name version))))
       ;; (modules '((guix build utils)))
       ;; (snippet
       ;;  `(begin
       ;;     ;; Remove bundled tarballs, patches, and .jars(!).  XXX: Upstream
       ;;     ;; claims to have fixed USE_EXTERNAL_YAML, but it still fails with:
       ;;     ;; https://github.com/AcademySoftwareFoundation/OpenColorIO/issues/517
       ;;     ;; When removing it, also remove it from the licence field comment.
       ;;     (for-each delete-file-recursively
       ;;               (filter
       ;;                (lambda (full-name)
       ;;                  (let ((file (basename full-name)))
       ;;                    (not (or (string-prefix? "yaml-cpp-0.3" file)
       ;;                             (string=? "unittest.h" file)))))
       ;;                (find-files "ext" ".*")))

       ;;     #t))))
    (build-system cmake-build-system)
    (arguments
     `(#:configure-flags
       (list (string-append "-DCMAKE_CXX_FLAGS="
                            "-Wno-error=deprecated-declarations "
                            "-Wno-error=unused-function")
             "-DOCIO_BUILD_STATIC=OFF")))
    (native-inputs
     `(("git" ,git)
       ("pkg-config" ,pkg-config)))
    (inputs
     ;; XXX Adding freeglut, glew, ilmbase, mesa, and openimageio for
     ;; ocioconvert fails: https://github.com/AcademySoftwareFoundation/OpenColorIO/issues/1309
     `(("lcms" ,lcms)
       ;; ("mesa" ,mesa)
       ;; ("glew" ,glew)
       ;; ("openimageio" ,openimageio)
       ;; ("freeglut" ,freeglut)
       ("openexr" ,openexr)
       ("tinyxml" ,tinyxml)
       ("expat" ,expat)
       ("yaml-cpp" ,yaml-cpp)
       ("pystring" ,pystring)
       ("python" ,python)
       ("pybind11" ,pybind11)))
    ;; (search-paths
    ;;  `(,(search-path-specification
    ;;      (variable "pystring_INCLUDE_DIR")
    ;;      (separator #f)
    ;;      (files (list "include/pystring")))))
    (home-page "https://opencolorio.org")
    (synopsis "Color management for visual effects and animation")
    (description
     "OpenColorIO, or OCIO, is a complete color management solution geared
towards motion picture production, with an emphasis on visual effects and
computer animation.  It provides a straightforward and consistent user
experience across all supporting applications while allowing for sophisticated
back-end configuration options suitable for high-end production usage.

OCIO is compatible with the @acronym{ACES, Academy Color Encoding
Specification} and is @acronym{LUT, look-up table}-format agnostic, supporting
many popular formats.")
    (license (list license:expat        ; docs/ociotheme/static, ext/yaml-cpp-*
                   license:zlib         ; src/core/md5
                   license:bsd-3))))
