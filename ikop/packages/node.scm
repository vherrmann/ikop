(define-module (ikop packages node))

(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix git-download)
             (guix build-system node)
             (gnu))

(use-package-modules node)

(define-public node-cross-env
  (package
    (name "node-cross-env")
    (version "7.0.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/kentcdodds/cross-env")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "070jx2si3z2z69pq80v4xdfigldzpfm37hsypj34z8jlv48v62az"))))
    (build-system node-build-system)
    (arguments
     `(#:node ,node))
    (home-page "https://github.com/kentcdodds/cross-env")
    (synopsis "")
    (description "")
    (license license:expat)))

(define-public node-cross-spawn
  (package
    (name "node-cross-spawn")
    (version "7.0.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/moxystudio/node-cross-spawn")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1if9540d6nb7w5wx3g3y9cphyjrzfdhfx2sgiiv8mlabibqf1szn"))))
    (build-system node-build-system)
    (home-page "https://github.com/moxystudio/node-cross-spawn")
    (synopsis "")
    (description "")
    (license license:expat)))

(define-public node-shebang-command
  (package
    (name "node-shebang-command")
    (version "2.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/kevva/shebang-command")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0jnv8ch4in3rvq3lg2mlsw7q1sfn9b86nxczy4nb4a112aqg1wy4"))))
    (arguments
     `(#:tests? #f))
    (inputs
     `(("node-shebang-regex" ,node-shebang-regex)))
    (build-system node-build-system)
    (home-page "https://github.com/kevva/shebang-command")
    (synopsis "")
    (description "")
    (license license:expat)))

(define-public node-shebang-regex
  (package
    (name "node-shebang-regex")
    (version "3.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/sindresorhus/shebang-regex")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1xy5niin5ll8acc3hmhvk8423vsp1i1bwz95d66wjr0hy10hj1vd"))))
    (arguments
     `(#:tests? #f))
    (build-system node-build-system)
    (home-page "https://github.com/sindresorhus/shebang-regex")
    (synopsis "")
    (description "")
    (license license:expat)))

(define-public node-path-key
  (package
    (name "node-path-key")
    (version "3.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/sindresorhus/path-key")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0man66sca5k4babicdfmf159iw9vma8r1kwdy4d06kn6n6pflb8k"))))
    (arguments
     `(#:tests? #f))
    (build-system node-build-system)
    (home-page "https://github.com/sindresorhus/path-key")
    (synopsis "")
    (description "")
    (license license:expat)))
