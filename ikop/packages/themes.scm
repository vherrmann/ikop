(define-module (ikop packages themes))

(use-modules (guix)
             (guix packages)
             (guix build-system meson)
             (guix build-system copy)
             ((guix licenses) #:prefix license:)
             (guix download)
             (ikop packages)
             (ikut git-download)
             (gnu))

(use-package-modules gtk
                     glib
                     gnome
                     python
                     web
                     pkg-config)

(define-public humanity-icon-theme
  (package
   (name "humanity-icon-theme")
   (version "0.6.15")
   (source
    (origin
     (method url-fetch)
     (uri (string-append "https://launchpad.net/ubuntu/+archive/primary/+files/"
                         name "_" version
                         ".tar.xz"))
     (sha256
      (base32
       "19ja47468s3jfabvakq9wknyfclfr31a9vd11p3mhapfq8jv9g4x"))))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan
       `(("." "share/icons"))))
   (home-page "https://launchpad.net/humanity/")
   (synopsis "Humanity icons from Ubuntu")
   (description "Humanity icons from Ubuntu")
   (license license:gpl2)))

(define-public yaru-theme
  (package
   (name "yaru-theme")
   (version "20.10.2")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url (github-url "ubuntu" "yaru"))
           (commit version)))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0vxs17nbahrdix1q9xj06nflm344lfgj2mrafsvyfcr2isn61iv6"))))
   (build-system meson-build-system)
   (native-inputs (auto-inputs pkg-config
                               ("glib-bin" glib "bin")
                               ("gtk+" gtk+ "bin")
                               sassc))
   (inputs (auto-inputs gtk+
                        gnome-themes-extra))
   (propagated-inputs (auto-inputs hicolor-icon-theme
                                   humanity-icon-theme))
   ;; (arguments
   ;;  `(#:phases
   ;;    (modify-phases %standard-phases
   ;;                   (add-after 'patch 'patch-shebang
   ;;                              (lambda _
   ;;                                (patch-shebang ".")
   ;;                                #t)))))
   (home-page "https://github.com/ubuntu/yaru")
   (synopsis "Ubuntu community theme 'yaru' - default Ubuntu theme since 18.10")
   (description "Ubuntu community theme 'yaru' - default Ubuntu theme since 18.10")
   (license (list license:cc-by-sa4.0
                  license:gpl3))))
