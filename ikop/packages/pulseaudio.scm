(define-module (ikop packages pulseaudio))

(use-modules (guix)
             (guix packages)
             (guix git-download)
             (guix build-system meson)
             ((guix licenses) #:prefix license:)
             (guix utils)
             (gnu))

(use-package-modules pulseaudio
                     glib
                     gtk
                     gstreamer
                     audio
                     rdf
                     boost
                     algebra
                     xml
                     python
                     freedesktop
                     gettext
                     pkg-config
                     music
                     gcc)

;; doesn't find it's plugins
(define-public pulseeffects
  (package
    (name "pulseeffects")
    (version "4.8.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/wwmm/pulseeffects")
                    (commit (string-append "v"
                                           version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0k5p5y3im7xnf0ikaghh56nfhirkdwf95c8fr17wasgdpw2m86i2"))))
    (build-system meson-build-system)
    (native-inputs
     `(("libxml2" ,libxml2)
       ("itstool" ,itstool)
       ("python" ,python)
       ("desktop-file-utils" ,desktop-file-utils)
       ("gettext" ,gnu-gettext)
       ("gcc" ,gcc-10)
       ("pkg-config" ,pkg-config)
       ("appstream-util" ,appstream-glib)))
    (inputs
     `(("pulseaudio" ,pulseaudio)
       ("glib" ,glib)
       ("glib" ,glib "bin")
       ("glibmm" ,glibmm)
       ("gtk+" ,gtk+)
       ("gtk+" ,gtk+ "bin")
       ("gtkmm" ,gtkmm)
       ("gstreamer" ,gstreamer)
       ("gst-plugins-base" ,gst-plugins-base)
       ("gst-plugins-good" ,gst-plugins-good)
       ("gst-plugins-bad" ,gst-plugins-bad)
       ("lilv" ,lilv)
       ("lv2" ,lv2)
       ("serd" ,serd)
       ("sord" ,sord)
       ("sratom" ,sratom)
       ("libbs2b" ,libbs2b)
       ("libebur128" ,libebur128)
       ("libsamplerate" ,libsamplerate)
       ("libsndfile" ,libsndfile)
       ("rnnoise" ,rnnoise)
       ("boost" ,boost)
       ("dbus" ,dbus)
       ("fftwf" ,fftwf)
       ("zita-convolver" ,zita-convolver)
       ("calf" ,calf)
       ("lsp-plugin" ,lsp-plugins)
       ("rubberband" ,rubberband)
       ("zam-plugins" ,zam-plugins)))
    (propagated-inputs
     `(("calf" ,calf)
       ("lsp-plugins" ,lsp-plugins)))
    ;; (native-search-paths
    ;;  (list (search-path-specification
    ;;         (variable "LV2_PATH")
    ;;         (files (list (assoc-ref %build-inputs "calf")
    ;;                      (assoc-ref %build-inputs "lsp-plugins"))))))
    (description "Limiter, compressor, reverberation, equalizer and auto volume effects for Pulseaudio applications")
    (synopsis "Limiter, compressor, reverberation, equalizer and auto volume effects for Pulseaudio applications")
    (home-page "https://github.com/wwmm/pulseeffects")
    (license license:gpl3)))
