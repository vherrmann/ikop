(define-module (ikop packages c++-xyz))

(use-modules (guix)
             (guix packages)
             (guix build-system gnu)
             ((guix licenses) #:prefix license:)
             (ikop packages)
             (ikut git-download)
             (gnu packages autotools))

(define-public pystring
  (package
   (name "pystring")
   (version "1.1.3")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url (github-url "imageworks" "pystring"))
           (commit "281419de2f91f9e0f2df6acddfea3b06a43436be")))
     (file-name (git-file-name name version))
     (patches (search-patches "pystring-install-headers.patch"))
     (sha256
      (base32
       "0vjd57y5lpmy1518lrwjilhj45d5fqhc5pjg1shzwv91iml3j3il"))))
   (build-system gnu-build-system)
   (native-inputs (auto-inputs libtool))
   (arguments
    `(#:phases
      (modify-phases %standard-phases
                     (delete 'configure)
                     (delete 'check)
                     (add-after 'unpack 'patch-libdir
                                (lambda* (#:key outputs #:allow-other-keys)
                                  (let [(out (assoc-ref outputs "out"))]
                                    (substitute* "Makefile"
                                                 (("PREFIX = /usr")
                                                  (string-append "PREFIX = "
                                                                 out))))
                                  #t))
                     (add-before 'install 'create-out-dir
                                 (lambda* (#:key outputs #:allow-other-keys)
                                   (let ((out (assoc-ref outputs "out")))
                                     (mkdir-p (string-append out "/lib"))))))))
   (home-page "https://github.com/imageworks/pystring")
   (synopsis "C++ functions matching the interface and behavior of python string methods with std::string ")
   (description "C++ functions matching the interface and behavior of python string methods with std::string ")
   (license license:bsd-3)))
