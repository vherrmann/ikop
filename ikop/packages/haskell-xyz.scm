(define-module (ikop packages haskell-xyz))

(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix build-system haskell)
             (guix download)
             (gnu packages)
             (gnu packages haskell-web)
             (gnu packages haskell-xyz)
             (gnu packages haskell-check))

(define-public ghc-ini
  (package
    (name "ghc-ini")
    (version "0.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/ini/ini-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0mvwii8jbh2ll54qb9dij5m66c6324s2y4vrwz1qr4wz40m3qa8l"))))
    (build-system haskell-build-system)
    (inputs
     `(("ghc-attoparsec" ,ghc-attoparsec)
       ("ghc-unordered-containers"
        ,ghc-unordered-containers)))
    (native-inputs `(("ghc-hspec" ,ghc-hspec)))
    (home-page "http://github.com/chrisdone/ini")
    (synopsis
     "Quick and easy configuration files in the INI format.")
    (description
     "Quick and easy configuration files in the INI format.")
    (license license:bsd-3)))

(define-public ghc-quickcheck-assertions
  (package
    (name "ghc-quickcheck-assertions")
    (version "0.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/quickcheck-assertions/quickcheck-assertions-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "1kyam4cy7qmnizjwjm8jamq43w7f0fs6ljfplwj0ib6wi2kjh0wv"))))
    (build-system haskell-build-system)
    (inputs
     `(("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-ieee754" ,ghc-ieee754)
       ("ghc-pretty-show" ,ghc-pretty-show)))
    (native-inputs `(("ghc-hspec" ,ghc-hspec)))
    (home-page
     "https://github.com/s9gf4ult/quickcheck-assertions")
    (synopsis "HUnit like assertions for QuickCheck")
    (description
     "Library with convenient assertions for QuickCheck properties like in HUnit")
    (license license:lgpl3)))

(define-public ghc-test-framework-smallcheck
  (package
    (name "ghc-test-framework-smallcheck")
    (version "0.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/test-framework-smallcheck/test-framework-smallcheck-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "1xpgpk1gp4w7w46b4rhj80fa0bcyz8asj2dcjb5x1c37b7rw90b0"))))
    (build-system haskell-build-system)
    (inputs
     `(("ghc-test-framework" ,ghc-test-framework)
       ("ghc-smallcheck" ,ghc-smallcheck)))
    (home-page
     "https://github.com/feuerbach/smallcheck")
    (synopsis
     "SmallCheck support for the test-framework package. Support for SmallCheck tests in test-framework")
    (description
     "Support for SmallCheck tests in test-framework")
    (license license:bsd-3)))

(define-public ghc-word-wrap
  (package
    (name "ghc-word-wrap")
    (version "0.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/word-wrap/word-wrap-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "15rcqhg9vb7qisk9ryjnyhhfgigxksnkrczycaw2rin08wczjwpb"))))
    (build-system haskell-build-system)
    (native-inputs `(("ghc-hspec" ,ghc-hspec)))
    (arguments
     `(#:cabal-revision
       ("1"
        "1k4w4g053vhmpp08542hrqaw81p3p35i567xgdarqmpghfrk68pp")))
    (home-page
     "https://github.com/jtdaugherty/word-wrap/")
    (synopsis "A library for word-wrapping")
    (description
     "A library for wrapping long lines of text.")
    (license license:bsd-3)))

(define-public ghc-data-clist
  (package
    (name "ghc-data-clist")
    (version "0.1.2.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/data-clist/data-clist-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "1mwfhnmvi3vicyjzl33m6pcipi2v887zazyqxygq258ndd010s9m"))))
    (build-system haskell-build-system)
    (inputs `(("ghc-quickcheck" ,ghc-quickcheck)))
    (arguments
     `(#:cabal-revision
       ("1"
        "13hg7a3d4ky8b765dl03ryxg28lq8iaqj5ky3j51r0i1i4f2a9hy")))
    (home-page
     "https://github.com/sw17ch/data-clist")
    (synopsis "Simple functional ring type.")
    (description
     "Simple functional bidirectional ring type. Given that the ring terminiology clashes with certain mathematical branches, we're using the term CList or CircularList instead.")
    (license license:bsd-3)))

(define-public ghc-config-ini
  (package
    (name "ghc-config-ini")
    (version "0.2.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/config-ini/config-ini-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0dfm4xb1sd713rcqzplzdgw68fyhj24i6lj8j3q8kldpmkl98lbf"))))
    (build-system haskell-build-system)
    (inputs
     `(("ghc-unordered-containers"
        ,ghc-unordered-containers)
       ("ghc-megaparsec" ,ghc-megaparsec)))
    (native-inputs
     `(("ghc-ini" ,ghc-ini)
       ("ghc-hedgehog" ,ghc-hedgehog)
       ("ghc-doctest" ,ghc-doctest)
       ("ghc-microlens" ,ghc-microlens)))
    (arguments
     `(#:cabal-revision
       ("2"
        "0iwraaa0y1b3xdsg760j1wpylkqshky0k2djcg0k4s97lrwqpbcz")))
    (home-page
     "https://github.com/aisamanra/config-ini")
    (synopsis
     "A library for simple INI-based configuration files.")
    (description
     "The @config-ini@ library is a set of small monadic languages for writing simple configuration languages with convenient, human-readable error messages. . > parseConfig :: IniParser (Text, Int, Bool) > parseConfig = section \"NETWORK\" $ do >   user <- field        \"user\" >   port <- fieldOf      \"port\" number >   enc  <- fieldFlagDef \"encryption\" True >   return (user, port, enc)")
    (license license:bsd-3)))


(define-public ghc-vty
  (package
    (name "ghc-vty")
    (version "5.32")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/vty/vty-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0ydbifik7xilb33phglpjkgf6r8vifipyyq0wb6111azzj7dmszs"))))
    (build-system haskell-build-system)
    (inputs
     `(("ghc-blaze-builder" ,ghc-blaze-builder)
       ("ghc-microlens" ,ghc-microlens)
       ("ghc-microlens-mtl" ,ghc-microlens-mtl)
       ("ghc-microlens-th" ,ghc-microlens-th)
       ("ghc-hashable" ,ghc-hashable)
       ("ghc-parallel" ,ghc-parallel)
       ("ghc-utf8-string" ,ghc-utf8-string)
       ("ghc-vector" ,ghc-vector)
       ("ghc-ansi-terminal" ,ghc-ansi-terminal)))
    (native-inputs
     `(("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-smallcheck" ,ghc-smallcheck)
       ("ghc-quickcheck-assertions"
        ,ghc-quickcheck-assertions)
       ("ghc-test-framework" ,ghc-test-framework)
       ("ghc-test-framework-smallcheck"
        ,ghc-test-framework-smallcheck)
       ("ghc-random" ,ghc-random)
       ("ghc-hunit" ,ghc-hunit)
       ("ghc-quickcheck" ,ghc-quickcheck)
       ("ghc-smallcheck" ,ghc-smallcheck)
       ("ghc-quickcheck-assertions"
        ,ghc-quickcheck-assertions)
       ("ghc-test-framework" ,ghc-test-framework)
       ("ghc-test-framework-smallcheck"
        ,ghc-test-framework-smallcheck)
       ("ghc-test-framework-hunit"
        ,ghc-test-framework-hunit)
       ("ghc-random" ,ghc-random)
       ("ghc-string-qq" ,ghc-string-qq)))
    (home-page "https://github.com/jtdaugherty/vty")
    (synopsis "A simple terminal UI library")
    (description
     "vty is terminal GUI library in the niche of ncurses. It is intended to be easy to use, have no confusing corner cases, and good support for common terminal types. . See the @vty-examples@ package as well as the program @test/interactive_terminal_test.hs@ included in the @vty@ package for examples on how to use the library. . Import the \"Graphics.Vty\" convenience module to get access to the core parts of the library. . &#169; 2006-2007 Stefan O'Rear; BSD3 license. . &#169; Corey O'Connor; BSD3 license. . &#169; Jonathan Daugherty; BSD3 license.")
    (license license:bsd-3)))


(define-public ghc-text-zipper
  (package
    (name "ghc-text-zipper")
    (version "0.11")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/text-zipper/text-zipper-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "07l1pyx93gv95cn1wh1di129axhm9sqsn4znykliacv60ld854ys"))))
    (build-system haskell-build-system)
    (inputs `(("ghc-vector" ,ghc-vector)))
    (native-inputs
     `(("ghc-hspec" ,ghc-hspec)
       ("ghc-quickcheck" ,ghc-quickcheck)))
    (home-page
     "https://github.com/jtdaugherty/text-zipper/")
    (synopsis "A text editor zipper library")
    (description
     "This library provides a zipper and API for editing text.")
    (license license:bsd-3)))

(define-public ghc-brick
  (package
    (name "ghc-brick")
    (version "0.58.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/brick/brick-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0qsmj4469avxmbh7210msjwvz7fa98axxvf7198x0hb2y7vdpcz5"))))
    (build-system haskell-build-system)
    (inputs
     `(("ghc-vty" ,ghc-vty)
       ("ghc-data-clist" ,ghc-data-clist)
       ("ghc-dlist" ,ghc-dlist)
       ("ghc-exceptions" ,ghc-exceptions)
       ("ghc-microlens" ,ghc-microlens)
       ("ghc-microlens-th" ,ghc-microlens-th)
       ("ghc-microlens-mtl" ,ghc-microlens-mtl)
       ("ghc-config-ini" ,ghc-config-ini)
       ("ghc-vector" ,ghc-vector)
       ("ghc-contravariant" ,ghc-contravariant)
       ("ghc-text-zipper" ,ghc-text-zipper)
       ("ghc-word-wrap" ,ghc-word-wrap)
       ("ghc-random" ,ghc-random)))
    (native-inputs
     `(("ghc-quickcheck" ,ghc-quickcheck)))
    (home-page
     "https://github.com/jtdaugherty/brick/")
    (synopsis
     "A declarative terminal user interface library")
    (description
     "Write terminal user interfaces (TUIs) painlessly with 'brick'! You write an event handler and a drawing function and the library does the rest. . . > module Main where > > import Brick > > ui :: Widget () > ui = str \"Hello, world!\" > > main :: IO () > main = simpleMain ui . . To get started, see: . * <https://github.com/jtdaugherty/brick/blob/master/README.md The README> . * The <https://github.com/jtdaugherty/brick/blob/master/docs/guide.rst Brick user guide> . * The demonstration programs in the 'programs' directory . . This package deprecates <http://hackage.haskell.org/package/vty-ui vty-ui>.")
    (license license:bsd-3)))
